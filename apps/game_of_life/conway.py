import pygame, sys, time
from pygame.locals import *

pygame.init()

width = 640
height = 480
x_squares = 64
y_squares = 48
delay = 1

x_width = width/x_squares
y_width = height/y_squares

a = []
for i in xrange(x_squares):
	a.append([])
	for j in xrange(y_squares):
		a[i].append(0)

loop = True 
initialize = True
bgColor = pygame.Color(255,255,255)
blackColor = pygame.Color(0,0,0)

surfaceObject = pygame.display.set_mode((width, height))
surfaceObject.fill(bgColor)

for i in range(1, x_squares):
	pygame.draw.line(surfaceObject, blackColor, (i*x_width, 0), ( i*x_width, height), 1)

for j in range(1, y_squares):
	pygame.draw.line(surfaceObject, blackColor, (0, j*y_width), (width, j*y_width), 1)

pygame.display.set_caption('Conway\'s Game of Life')

while(initialize):
	for event in pygame.event.get():						
		if event.type == pygame.QUIT:
			sys.exit()
		elif event.type == pygame.MOUSEBUTTONUP:
			mouse_x, mouse_y =  pygame.mouse.get_pos()
			x_center = mouse_x/x_width
			y_center = mouse_y/y_width
			if(a[x_center][y_center] == 0):
				pygame.draw.rect(surfaceObject, blackColor, (x_center*x_width+1, y_center*y_width+1, x_width-1, y_width-1))
				a[x_center][y_center] = 1
			else:
				pygame.draw.rect(surfaceObject, bgColor, (x_center*x_width+1, y_center*y_width+1, x_width-1, y_width-1))
				a[x_center][y_center] = 0			
						
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_RETURN:
				initialize = False	
	pygame.display.update()

while(loop):
	old_a = []
	for i in xrange(x_squares):
		old_a.append([])
		for j in xrange(y_squares):
			old_a[i].append(a[i][j])

	for i in range(1,x_squares-1):
		for j in range(1,y_squares-1):
			num_of_neighbours = 0
			for p in range(i-1,i+2):				
				for q in range(j-1,j+2):
					if(p!=i or q!=j):
						if(old_a[p][q] == 1):
							num_of_neighbours = num_of_neighbours + 1
			if(old_a[i][j] == 1):
				if(num_of_neighbours<2):
					pygame.draw.rect(surfaceObject, bgColor, (i*x_width+1, j*y_width+1, x_width-1, y_width-1))
					a[i][j] = 0
				elif(num_of_neighbours>3):
					pygame.draw.rect(surfaceObject, bgColor, (i*x_width+1, j*y_width+1, x_width-1, y_width-1))
					a[i][j] = 0
				else:
					pygame.draw.rect(surfaceObject, blackColor, (i*x_width+1, j*y_width+1, x_width-1, y_width-1))
					a[i][j] = 1				 
			else:
				if(num_of_neighbours == 3):
					pygame.draw.rect(surfaceObject, blackColor, (i*x_width+1, j*y_width+1, x_width-1, y_width-1))
					a[i][j] = 1								
				
	for event in pygame.event.get():									
		if event.type == pygame.QUIT:
			sys.exit()
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				loop = False
	pygame.display.update()
	time.sleep(delay)

		
